<?php

namespace App\Libraries;

use App\Model\BusinessCaseItemRepository;
use App\Model\AddressRepository;

class PohodaManager
{
    /** @return \App\Model\BusinessCaseItemRepository */
    protected $businessCaseItemRepository;

    /** @return \App\Model\AddressRepository */
    protected $addressRepository;

    /** @return ICO config.neon */
    protected $ico;

    /** @return FTP config.neon */
    protected $ftp;

    public function __construct($ico, $ftp, BusinessCaseItemRepository $businessCaseItemRepository, AddressRepository $addressRepository)
    {
        $this->businessCaseItemRepository = $businessCaseItemRepository;
        $this->addressRepository = $addressRepository;
        $this->ico = $ico;
        $this->ftp = $ftp;
    }

    public function prepareCaseData($case)
    {
        $order = $case->toArray();
        $order['id'] = "OR-" . time() . "-" . $order['id'];
        $order['ico'] = $this->ico;
        $order['currency'] = $case->ref('currency_id')->name;
        $order['address'] = $case->ref('customer_id');
        $order['company'] = $case->ref('customer_id');
        $order['items'] = $this->businessCaseItemRepository->get($case->id);
        return $order;
    }

    public function prepareCustomerData($data)
    {
        $customer = $data->toArray();
        $customer['id'] = 'AD' . '-' . time() . '-' . $customer['id'];
        $customer['address'] = $this->addressRepository->get($customer['address_id'])->toArray();
        $customer['ico-anywhere'] = $this->ico;
        return $customer;
    }

    public function uploadToFTP($file)
    {
        $connection = ftp_connect($this->ftp['host'], $this->ftp['port']);
        ftp_login($connection, $this->ftp['user'], $this->ftp['pass']);
        ftp_pasv($connection, $this->ftp['passive']);
        if (ftp_put($connection, '/INPUT/' . $file, __DIR__ . '/../../temp/customers/' . $file, FTP_BINARY)) {
            ftp_close($connection);
            return true;
        } else {
            ftp_close($connection);
            return false;
        }
    }

    public function downloadFromFTP()
    {
        $connection = ftp_connect($this->ftp['host'], $this->ftp['port']);
        ftp_login($connection, $this->ftp['user'], $this->ftp['pass']);
        ftp_pasv($connection, $this->ftp['passive']);

        $fileListReponse = ftp_list($connection, 'RESPONSE');
        $this->responseFolderRead($connection, $fileListReponse);

        $fileListOutput = ftp_nlist($connection, 'OUTPUT');
        $this->outputFolderRead($connection, $fileListOutput);

        ftp_close($connection);
    }

    public function responseFolderRead($connection, $fileListResponse)
    {
        foreach ($fileListResponse as $file) {
            if (ftp_get($connection, __DIR__ . "/../../temp/cases/orders/response" . $file, "/RESPONSE/" . $file, FTP_BINARY)) {
                if (substr($file, -4) == ".xml") {
                    if ($this->parseXml($file)) {
                        ftp_delete($connection, "/RESPONSE/" . $file);
                        unlink(__DIR__ . "/../../temp/cases/orders" . $file);
                    }
                } else {
                    ftp_delete($connection, "/RESPONSE/" . $file);
                }
            }
        }
    }

    public function outputFolderRead($connection, $fileListOutput)
    {
        foreach ($fileListOutput as $file) {

        }
    }

    public function parseXML($file)
    {
        $xml = simplexml_load_file();
    }

}

