<?php

namespace App\Libraries;

use App\Model\BusinessCaseItemRepository;
use App\Model\CustomerRepository;
use App\Model\BusinessCaseCategoryRepository;
use App\Model\CurrencyRepository;

class CaseManager
{
    protected $businessItemRepository;
    protected $customerRepository;
    protected $businessCaseCategoryRepository;
    protected $currencyRepository;

    public function __construct(BusinessCaseItemRepository $businessItemRepository, CustomerRepository $customerRepository, BusinessCaseCategoryRepository $businessCaseCategoryRepository, CurrencyRepository $currencyRepository)
    {
        $this->businessItemRepository = $businessItemRepository;
	$this->customerRepository = $customerRepository;
	$this->businessCaseCategoryRepository = $businessCaseCategoryRepository;
	$this->currencyRepository = $currencyRepository;
    }
    
    public function getBusinessCases($cases): array 
    {
	$items = [];
	foreach($cases as $id => $case) {
	    $items[] = [
		'id' => $id,
		'name' => $case->name,
		'category' => $this->businessCaseCategoryRepository->fetch($case->business_case_category_id),
		'customer' => $this->customerRepository->fetch($case->customer_id),
		'state' => $this->setState($case),
		'created' => $case->date_created,
		'sum' => $this->sum($id) 
	    ];
	}
	return $items;
    }
    
    public function sum($id) 
    {
	$sum = [];
	$items = $this->businessItemRepository->get($id);
	foreach ($items as $id => $item) {
            if (isset($sum[$item->business_case_id])) {
                $sum[$item->business_case_id] += $item->price * $item->amount;
            } else {
                $sum[$item->business_case_id] = $item->price * $item->amount;
            }
        }
        return $sum;
    }

    public function setState($case) 
    {
	$state = false;
	if ($case->cost_exhibit) {
	    $state = 'Nákladový exhibit';
	} elseif ($case->internal_exhibit) {
	    $state = 'Interní exhibit';
	} elseif ($case->date_invoice && $case->date_paid === null) {
	    $state = 'Vyfakturováno';
	} elseif ($case->date_invoice && $case->date_paid) {
	    $state = 'Uhrazeno';
	} elseif ($case->date_created && $case->date_paid === null && $case->date_invoice === null) {
	    $state = 'Žaloženo';
	}
	
	return $state;
    }
    
    public function getItemsId($items)
    {
        $casesId = [];
        foreach ($items as $item) {
            $casesId[] = $item->id;
        }
        return $casesId;
    }

    public function sumPrice($items)
    {
        $sum = [];
        foreach ($items as $item) {
            $sum[] = $item->price * $item->amount;
        }
        $sumAll = array_sum($sum);

        return $sumAll;
    }

    public function getVat($items)
    {
        $vat = [];
        foreach ($items as $item) {
            $vat = $item->taxes_id;
        }

        return $vat;
    }

    public function sumPriceVat($price, $vat)
    {
        $priceVat = ($price * $vat) + $price;
        return $priceVat;
    }
}