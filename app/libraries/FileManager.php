<?php

namespace App\Libraries;

use Nette\Utils\FileSystem;

class FileManager
{

    public $dir;

    public $tempDir;

    public $invoiceDir;

    public $formater;

    public function __construct($tempDir, $dir, $invoiceDir, Formater $formater)
    {
        $this->dir = $dir;
        $this->tempDir = $tempDir;
        $this->invoiceDir = $invoiceDir;
        $this->formater = $formater;
    }

    public function deleteReceivedInvoiceFile($invoiceNumber)
    {
        $path = $this->invoiceDir . '/' . $invoiceNumber;
        FileSystem::delete($path);
    }

    public function deleteBusinessCaseFile($caseId)
    {
        $path = $this->dir . '/attachments/' . $caseId;
        FileSystem::delete($path);
    }

    public function uploadReceivedInvoice($file, $invoiceNumber, $form)
    {
        $path = $this->invoiceDir . $invoiceNumber;
        $file_path = $path . '/' . $file->getSanitizedName();

        if ($file->isOk()) {
            if (file_exists($path)) {
                FileSystem::delete($path . '/' . $form['upload']->getOption('description'));
                $file->move($file_path);
            } else {
                mkdir($path);
                $file->move($file_path);
            }
        }
    }

    public function uploadBusinessCaseAttachment($file, $businessCaseId, $form)
    {
        $path = $this->dir . '/attachments/' . $businessCaseId . '/';
        $file_path = $path . '/' . $file->getSanitizedName();
        $temp_file = $this->tempDir . '/attachments/' . $file->getSanitizedName();

        if ($file->isOk()) {
            if (file_exists($path)) {
                FileSystem::delete($path . '/' . $form['attachment']->getOption('description'));
                $file->move($file_path);
            } else {
                FileSystem::createDir($path);
                $file->move($file_path);
                FileSystem::delete($temp_file);
            }
        }
    }

    public function getNameFile($file)
    {
        return $this->formater->formatUrl($file);
    }
}
