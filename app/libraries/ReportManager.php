<?php

namespace App\Libraries;

use App\Model\BusinessCaseRepository;

class ReportManager
{
    /** @var BusinessCaseRepository */
    public $businessCaseRepository;

    public function __construct(BusinessCaseRepository $businessCaseRepository)
    {
        $this->businessCaseRepository = $businessCaseRepository;
    }

    public function loadReport($year, $quarter)
    {

    }

    public function getQuarter($year, $quarter)
    {
        $dateForQuarter = array(
            '2015' => array(
                'q1' => array('from' => '2015-01-01', 'to' => '2015-04-10'),
                'q2' => array('from' => '2015-04-10', 'to' => '2015-07-10'),
                'q3' => array('from' => '2015-07-10', 'to' => '2015-10-10'),
                'q4' => array('from' => '2015-10-10', 'to' => '2016-01-10')),
            '2016' => array(
                'q1' => array('from' => '2016-01-10', 'to' => '2016-04-10'),
                'q2' => array('from' => '2016-04-10', 'to' => '2016-07-10'),
                'q3' => array('from' => '2016-07-10', 'to' => '2016-10-10'),
                'q4' => array('from' => '2016-10-10', 'to' => '2017-01-10')),
            '2017' => array(
                'q1' => array('from' => '2017-01-10', 'to' => '2017-04-10'),
                'q2' => array('from' => '2017-04-10', 'to' => '2017-07-10'),
                'q3' => array('from' => '2017-07-10', 'to' => '2017-10-10'),
                'q4' => array('from' => '2017-10-10', 'to' => '2017-12-31')),
            '2018' => array(
                'q1' => array('from' => '2018-01-01', 'to' => '2018-03-31'),
                'q2' => array('from' => '2018-04-01', 'to' => '2018-06-30'),
                'q3' => array('from' => '2018-07-01', 'to' => '2018-09-30'),
                'q4' => array('from' => '2018-10-01', 'to' => '2018-12-31')
            ));

        if (array_key_exists($year, $dateForQuarter)) {
            if (array_key_exists($quarter, $dateForQuarter[$year])) {
                return $dateForQuarter[$year][$quarter];
            } else {
                return array('from' => null, 'to' => null);
            }
        } else {
            return array('from' => null, 'to' => null);
        }
    }
}

