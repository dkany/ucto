<?php

namespace App\Libraries;

class Formater
{
    public function formatUrl($file)
    {
        if (is_string($file)) {
            return $file;
        } else {
            return $file->getSanitizedName();
        }
    }
}
