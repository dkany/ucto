<?php

namespace App\Presenters;

class PaidInvoicePresenter extends SecuredPresenter
{

    /** @var \App\Libraries\Formater @inject */
    public $formater;

    /** @var \App\Model\PaidInvoiceRepository @inject */
    public $paidInvoiceRepository;

    public function renderDefault()
    {
        $invoices = $this->paidInvoiceRepository->getInvoicesToPay();
        $this->template->invoices = $invoices;
    }

    public function actionPay($id)
    {
        $this->paidInvoiceRepository->paid($id);
        $this->flashMessage('Faktura byla uhrazena.', 'alert alert-success');
        $this->redirect('default');
    }
}
