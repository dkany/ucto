<?php

namespace App\Presenters;

class ApprovalInvoicePresenter extends SecuredPresenter
{

    /** @var \App\Libraries\Formater @inject */
    public $formater;

    /** @var \App\Model\ApprovalInvoiceRepository @inject */
    public $approvalInvoiceRepository;

    public function renderDefault()
    {
        $invoices = $this->approvalInvoiceRepository->getInvoicesToSign($this->user->getId());
        $this->template->invoices = $invoices;
    }

    public function actionApprove($id)
    {
        $this->approvalInvoiceRepository->approve($id);
        $this->flashMessage('Faktura byla úspěšně schválená', 'alert alert-success');
        $this->redirect('default');
    }
    
    public function actionDecline($id)
    {
	$this->approvalInvoiceRepository->decline($id);
	$this->flashMessage('Faktura odmítnuta', 'alert alert-success');
        $this->redirect('default');
    }
}
