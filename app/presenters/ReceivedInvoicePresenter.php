<?php

namespace App\Presenters;

class ReceivedInvoicePresenter extends SecuredPresenter
{

    /** @persistent */
    public $year;

    /** @persistent */
    public $month;

    private $id;
    
    /** @var \App\Forms\FormFactory @inject */
    public $formFactory;

    /** @var \App\Libraries\Formater @inject */
    public $formater;

    /** @var \App\Libraries\FileManager @inject */
    public $fileManager;

    /** @var \App\Forms\InvoiceForm @inject */
    public $invoiceForm;

    /** @var \App\Forms\FilterForm @inject */
    public $filterForm;

    /** @var \App\Model\ReceivedInvoiceRepository @inject */
    public $receivedInvoiceRepository;

    /** @var \App\Model\CustomerRepository @inject */
    public $customerRepository;

    /** @var \App\Model\CurrencyRepository @inject */
    public $currencyRepository;

    /** @var \App\Model\UserRepository @inject */
    public $userRepository;
    
    /** @var \App\Model\ApprovalInvoiceRepository @inject */
    public $approvalInvoiceRepository;

    public function renderDefault()
    {
        if ($this->year == null) {
            $this->year = date('Y', time());
        }

        $this->session->getSection('summaryInvoice')->remove();
        $this['filterForm']->setDefaults(['year' => $this->year]);
        $invoices = $this->filterForm->applyFilters($this->year);
        $this->template->received_invoices = $invoices;
    }

    protected function createComponentInvoiceForm()
    {
        $form = $this->invoiceForm->create($this->id);
        $form->onSuccess[] = function () {
            $this->redirect('summary', $this->id);
        };
        return $form;
    }

    protected function createComponentFilterForm()
    {
        $form = $this->filterForm->create();
        $form->onSuccess[] = function (\Nette\Application\UI\Form $form) {
            $this->year = $form->values->year;
            $this->redirect('default');
        };
        return $form;
    }
    
    protected function createComponentRejectInvoiceForm()
    {
        $form = $this->formFactory->create();
        $form->addTextArea('note', 'Poznámka k odmítnutí');
        $form->addSubmit('send', 'Potvrdit odmítnutí')
                ->setAttribute('class', 'pull-right');
        $form->onSuccess[] = [$this, 'rejectWithNotice'];
	return $form;
    }
    
    public function rejectWithNotice($form, $values)
    {
	$this->approvalInvoiceRepository->reject($this->getParameter('id'), $values->note);
	$this->flashMessage('Přijatá faktura byla odmítnuta.', 'alert alert-success');
	$this->redirect('default');
    }
    
    protected function createComponentNewSignedUserForm()
    {
        $form = $this->formFactory->create();
	$form->addMultiSelect('approve_user_id', 'Uživatel', $this->userRepository->fetchPairs());
        $form->addSubmit('send', 'Předat k podpisu')
                ->setAttribute('class', 'pull-right');
        $form->onSuccess[] = [$this, 'newSignedUser'];
	
	return $form;
    }
    
    public function newSignedUser($form, $values)
    {
	$this->approvalInvoiceRepository->changeOfApproval($this->getParameter('id'), $values->approve_user_id);
	$this->flashMessage('Změna schvalovatele proběhla úspěšně.', 'alert alert-success');
	$this->redirect('default');
    }


    public function renderDetail($id)
    {
        $invoice = $this->receivedInvoiceRepository->get($id);
        $this->template->detail = $invoice;
        $this->template->customer = $this->customerRepository->fetch($invoice->customer_id);
        $this->template->currency = $this->currencyRepository->fetch($invoice->currency_id);
        $this->template->created = $this->userRepository->fetch($invoice->created_user_id);
        $this->template->file = $this->fileManager->invoiceDir . $invoice->invoice_number . '/' . $invoice->upload;
        $this->template->approve = $this->userRepository->fetch($invoice->approve_user_id);
    }

    public function renderSummary($id)
    {
        $session = $this->session->getSection('summaryInvoice')->values;
        $this->template->id = $id;
        $this->template->summaryInvoice = $session;
        $this->template->customer = $this->customerRepository->fetch($session->customer_id);
        $this->template->currency = $this->currencyRepository->fetch($session->currency_id);
        $this->template->created = $this->userRepository->fetch($session->created_user_id);
        $this->template->file = $this->fileManager->invoiceDir . $session->invoice_number . '/' . $session->upload;
        $this->template->approve = $this->userRepository->fetch($session->approve_user_id);
    }

    public function actionSave($id)
    {
        if ($id) {
            $this->session->getSection('summaryInvoice')->values->upload = $this->formater->formatUrl($this->session->getSection('summaryInvoice')->values->upload);
            $this->receivedInvoiceRepository->update($id, $this->session->getSection('summaryInvoice')->values);
            $this->flashMessage('Faktura byla úspěšně aktualizována', 'alert alert-success');
        } else {
            $this->session->getSection('summaryInvoice')->values->state = 'Čeká na schválení';
            $this->session->getSection('summaryInvoice')->values->based = date('Y-m-d H:i:s');
            $this->session->getSection('summaryInvoice')->values->upload = $this->formater->formatUrl($this->session->getSection('summaryInvoice')->values->upload);
            $this->session->getSection('summaryInvoice')->values->created_user_id = $this->user->getId();
            $this->receivedInvoiceRepository->insert($this->session->getSection('summaryInvoice')->values);
            $this->flashMessage('Faktura byla úspěšně vytvořena', 'alert alert-success');
        }

        $this->redirect('default');
    }

    public function actionEdit($id)
    {
        $this->id = (int)$id;
        $session = $this->session->getSection('summaryInvoice')->values;

        if ($id && empty($session)) {
            $invoice = $this->receivedInvoiceRepository->get($id);
            $this['invoiceForm']->setDefaults($invoice);
            $this['invoiceForm']['upload']->setOption('description', $invoice->upload)->setRequired(false);
            $this['invoiceForm']['receive_date']->setDefaultValue($invoice->receive_date->format('Y-m-d'));
            $this['invoiceForm']['due_date']->setDefaultValue($invoice->due_date->format('Y-m-d'));
        } elseif ($session) {
            $this['invoiceForm']['upload']->setOption('description', $session->upload)->setRequired(false);
            $this['invoiceForm']->setDefaults($session);
        } else {
            $this->error('Faktura nebyla nalezena', 'alert alert-danger');
        }
    }

    public function actionDownload($file)
    {
        $this->sendResponse(new \Nette\Application\Responses\FileResponse($file));
    }

    public function handleDelete($id)
    {
        $invoice = $this->receivedInvoiceRepository->get($id);
        $this->fileManager->deleteReceivedInvoiceFile($invoice->invoice_number);
        $invoice->delete();

        $this->flashMessage('Přijatá faktura byla úspěšně smazána', 'alert alert-success');
        if (!$this->isAjax()) {
            $this->redirect('this');
        }
    }

    public function actionCommand($id)
    {
        $this->receivedInvoiceRepository->command($id);
        $this->flashMessage('Faktura má nastavený příkaz k úhradě.', 'alert alert-success');
        $this->redirect('default');
    }
}
