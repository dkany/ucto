<?php

namespace App\Presenters;

class SignPresenter extends BasePresenter
{

    /** @var \App\Forms\SignInForm @inject */
    public $signInForm;

    /** @var \App\Forms\SignUpForm @inject */
    public $signUpForm;

    public function beforeRender()
    {
        parent::beforeRender();
        if ($this->getUser()->isLoggedIn()) {
            $this->redirect('Homepage:default');
        }
    }

    public function createComponentSignInForm()
    {
        return $this->signInForm->create(function () {
            $this->redirect('Homepage:default');
        });
    }

    public function createComponentSignUpForm()
    {
        return $this->signUpForm->create(function () {
            $this->flashMessage('Registrace proběhla úspěšně.', 'alert alert-success');
            $this->redirect('Sign:in');
        });
    }

    public function actionOut()
    {
        $this->getUser()->logout();
        $this->flashMessage('Byli jste úspěšně odhlášeni', 'alert alert-success');
        $this->redirect('Sign:in');
    }
}
