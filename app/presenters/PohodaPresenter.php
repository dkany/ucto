<?php

namespace App\Presenters;

class PohodaPresenter extends BusinessPresenter
{
    /** @var \App\Model\BusinessCaseRepository @inject */
    public $businessCaseRepository;

    /** @var \App\Model\BusinessCaseItemRepository @inject */
    public $businessCaseItemRepository;

    /** @var \App\Model\CustomerRepository @inject */
    public $customerRepository;
    
    /** @var \App\Model\PohodaRepository @inject */
    public $pohodaRepository;

    /** @var \App\Libraries\PohodaManager @inject */
    public $pohodaManager;

    public function actionSendOrderToPohoda($id)
    {
        $businessCase = $this->businessCaseRepository->get($id);
        if ($businessCase->cost_exhibit !== true) {
            $orderXML = $this->pohodaManager->prepareCaseData($businessCase);    
            $template = $this->createTemplate()->setFile(__DIR__ . '/templates/XML/order.latte');
            $template->order = $orderXML;
            $order = array('template' => (string)$template, 'id' => $orderXML['id']);
            $file = $order['id'] . '.xml';
	        $this->pohodaRepository->insertOrderData($order, $this->user->id, $id);
            if (file_put_contents(__DIR__ . '/../../temp/cases/orders/' . $file, $order['template'])) {
//		if($this->pohodaManager->uploadFTP($file)) {
		    $this->flashMessage('Obchodní případ byl uložen a odeslán do Pohody', 'alert alert-success');
		    $this->redirect('Business:default');
//		}		
            } else {
                $this->error('Chyba při vytváření souboru XML');
            }
        } else {
            $this->flashMessage('Obchodní případ byl uložen', 'alert alert-success');
            $this->redirect('Business:default');
        }
    }

    public function actionSendCustomerToPohoda($id)
    {
        $customer = $this->customerRepository->get($id);
        $customerXML = $this->pohodaManager->prepareCustomerData($customer);
        $template = $this->createTemplate()->setFile(__DIR__ . '/templates/XML/customer.latte');
        $template->customer = $customerXML;
        $template->ico = $customerXML['ico-anywhere'];
        $template->address = $customerXML['address'];
        $data = array('template' => (string)$template, 'id' => $customerXML['id']);
        $file = $data['id'] . '.xml';
	$this->pohodaRepository->insertCustomerDate($data, $this->user->id);
        if (file_put_contents(__DIR__ . '/../../temp/customers/' . $file, $data['template'])) {
//	    if($this->pohodaManager->uploadFTP($file)) {
		$this->flashMessage('Zákazník byl úspěšně uložen a odeslán do Pohody.', 'alert alert-success');
		$this->redirect('Customer:default');
//	    }
        } else {
            $this->error('Chyba při vytváření XML zákazníka.');
        }
    }

}