<?php

namespace App\Presenters;

abstract class SecuredPresenter extends BasePresenter
{

    protected function startup()
    {
        parent::startup();
        if (!$this->isAllowed()) {
            $this->redirect('Sign:in');
        }
    }

    protected function isAllowed(): bool
    {
        return $this->getUser()->isLoggedIn();
    }
}
