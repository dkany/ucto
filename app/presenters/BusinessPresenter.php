<?php

namespace App\Presenters;

use Nette\Utils\Arrays;

class BusinessPresenter extends SecuredPresenter
{
    private $id;

    /** @var \App\Libraries\CaseManager @inject */
    public $caseManager;

    /** @var \App\Libraries\Formater @inject */
    public $formater;

    /** @var \App\Libraries\FileManager @inject */
    public $fileManager;

    /** @var \App\Model\BusinessCaseRepository @inject */
    public $businessCaseRepository;

    /** @var \App\Forms\BusinessCaseForm @inject */
    public $businessCaseForm;

    /** @var \App\Model\SellerRepository @inject */
    public $sellerRepository;

    /** @var \App\Model\CustomerRepository @inject */
    public $customerRepository;

    /** @var \App\Model\CurrencyRepository @inject */
    public $currencyRepository;

    /** @var \App\Model\BusinessCaseCategoryRepository @inject */
    public $businessCaseCategoryRepository;

    /** @var \App\Model\BusinessCaseItemRepository @inject */
    public $businessCaseItemRepository;

    /** @var \App\Model\TaxRepository @inject */
    public $taxRepository;

    /** @var \App\Libraries\PohodaManager @inject */
    public $pohodaManager;

    public function renderDefault()
    {
        $this->session->getSection('summaryBusinessCase')->remove();
        $this->session->getSection('summaryBusinessCaseItem')->remove();
        $this->template->cases = $this->caseManager->getBusinessCases($this->businessCaseRepository->getAll());
    }

    public function renderSummary($id)
    {
        $businessCase = $this->session->getSection('summaryBusinessCase')->values;
        $businessItem = $this->session->getSection('summaryBusinessCaseItem')->values;

        if ($businessCase->cost_exhibit !== true) {
            $vat = $this->caseManager->getVat($businessItem);
            $tax = $this->taxRepository->get($vat);
            $this->template->sumPrice = $this->caseManager->sumPrice($businessItem);
            $this->template->sumPriceVat = $this->caseManager->sumPriceVat($this->caseManager->sumPrice($businessItem), $tax->vat);
            $this->template->items = $businessItem;
        }

        $this->template->id = $id;
        $this->template->summary = $businessCase;
        $this->template->currency = $this->currencyRepository->fetch($businessCase->currency_id);
        $this->template->address = $this->customerRepository->getAdrress($businessCase->customer_id);
        $this->template->seller = $this->sellerRepository->getSeller($businessCase->user_id);
        $this->template->category = $this->businessCaseCategoryRepository->getBusinessCaseCategory($businessCase->business_case_category_id);
        $this->template->customer = $this->customerRepository->get($businessCase->customer_id);
        $this->template->files = $businessCase->attachment;
    }

    public function renderDetail($id)
    {
        $businessCase = $this->businessCaseRepository->get($id);
        $businessItem = $this->businessCaseItemRepository->get($id);

        if ($businessCase->cost_exhibit !== 1 && $businessItem->count() !== null) {
            $vat = $this->caseManager->getVat($businessItem);
            $tax = $this->taxRepository->get($vat);
            $this->template->sumPrice = $this->caseManager->sumPrice($businessItem);
            $this->template->sumPriceVat = $this->caseManager->sumPriceVat($this->caseManager->sumPrice($businessItem), $tax->vat);
            $this->template->items = $businessItem;
        }

        $this->template->data = $businessCase;
        $this->template->currency = $this->currencyRepository->fetch($businessCase->currency_id);
        $this->template->address = $this->customerRepository->getAdrress($businessCase->customer_id);
        $this->template->seller = $this->sellerRepository->getSeller($businessCase->user_id);
        $this->template->category = $this->businessCaseCategoryRepository->getBusinessCaseCategory($businessCase->business_case_category_id);
        $this->template->customer = $this->customerRepository->get($businessCase->customer_id);
        $this->template->file_path = $this->fileManager->dir . '/attachments/' . $id . '/' . $businessCase->attachment;
        $this->template->file = $businessCase->attachment;
        $this->template->state = $this->caseManager->setState($businessCase);
    }

    public function createComponentBusinessCaseForm()
    {
        $form = $this->businessCaseForm->create($this->id, $this);
        return $form;
    }

    public function actionSaveBusinessCase($id, $send = false)
    {
        $businessCase = $this->session->getSection('summaryBusinessCase')->values;
        $businessItems = $this->session->getSection('summaryBusinessCaseItem')->values;

        if ($id) {
            if (!is_string($businessCase->attachment) && $businessCase->attachment !== null) {
                if ($businessCase->attachment->isOk()) {
                    $this->fileManager->uploadBusinessCaseAttachment($businessCase->attachment, $id, $this['businessCaseForm']);
                    $businessCase->attachment = $businessCase->attachment->getSanitizedName();
                }
            }

            $this->businessCaseRepository->update($id, $businessCase);

            if ($businessCase->cost_exhibit == false) {
                $sessionItemsId = $this->caseManager->getItemsId($businessItems);
                $caseId = $this->businessCaseItemRepository->get($id);
                $casesIdDb = $this->caseManager->getItemsId($caseId);

                $array_del = array_diff($casesIdDb, $sessionItemsId);

                if ($array_del) {
                    foreach ($array_del as $deleteItems) {
                        $this->businessCaseItemRepository->deleteItem($deleteItems);
                    }
                }

                if ($businessItems) {
                    foreach ($businessItems as $item) {
                        if (!$item['id'] && !in_array($item['id'], $casesIdDb)) {
                            $this->businessCaseItemRepository->insertItem($id, $item);
                        } else {
                            $this->businessCaseItemRepository->update($item['id'], $item);
                        }
                    }
                }

                if ($send === true) {
                    $this->redirect('Pohoda:sendOrderToPohoda', $id);
                }
            }
            $this->flashMessage('Obchodní případ byl úspěšně aktualizován', 'alert alert-success');
            $this->redirect('Business:default');

        } else {
            $case = $this->businessCaseRepository->insert($businessCase);

            if ($businessCase->attachment instanceof \Nette\Http\FileUpload) {
                $this->fileManager->uploadBusinessCaseAttachment($businessCase->attachment, $case, $this['businessCaseForm']);
            }

            if ($businessItems) {
                foreach ($businessItems as $item) {
                    $this->businessCaseItemRepository->insertItem($case, $item);
                }
            }

            if ($send === true) {
                $this->redirect('Pohoda:sendOrderToPohoda', $case->id);
            }

            $this->flashMessage('Obchodní případ byl úspěšně vytvořen', 'alert alert-success');
            $this->redirect('Business:default');
        }
    }


    public function actionEdit($id)
    {
        $this->id = $id;
        $businessCase = $this->session->getSection('summaryBusinessCase')->values;
        $businessItem = $this->session->getSection('summaryBusinessCaseItem')->values;

        if ($id && empty($businessCase) && empty($businessItem)) {
            $busCase = $this->businessCaseRepository->get($id);
            $busItem = $this->businessCaseItemRepository->get($id);

            $i = 0;
            $items = [];
            foreach ($busItem as $item) {
                $items[$i] = $item;
                $i++;
            }

            $this['businessCaseForm']['businessCaseItems']->setDefaults($items);
            $this['businessCaseForm']->setDefaults($busCase);
            $this['businessCaseForm']['attachment']->setOption('description', $busCase->attachment)->setRequired(false);
            $this['businessCaseForm']['date_created']->setDefaultValue($busCase->date_created->format('Y-m-d'));
            $this['businessCaseForm']['due_date']->setDefaultValue($busCase->due_date->format('Y-m-d'));
        } elseif (!empty($businessCase)) {
            $this['businessCaseForm']['attachment']->setOption('description', $businessCase->attachment)->setRequired(false);
            if ($businessItem) {
                $this['businessCaseForm']['businessCaseItems']->setDefaults($businessItem);
            }
            $this['businessCaseForm']->setDefaults($businessCase);
        } else {
            $this->error('Faktura nebyla nalezena');
        }
    }

    public function handleDelete($id)
    {
        $businessCase = $this->businessCaseRepository->get($id);
        $this->fileManager->deleteBusinessCaseFile($id);
        $this->businessCaseItemRepository->deleteItemsCase($id);
        $businessCase->delete();

        $this->flashMessage('Obchodní případ byl úspěšně smazána', 'alert alert-success');
        if (!$this->isAjax()) {
            $this->redirect('this');
        }
    }

    public function actionDownload($file)
    {
        $this->sendResponse(new \Nette\Application\Responses\FileResponse($file));
    }
}