<?php

namespace App\Presenters;

use Nette\Utils\Arrays;

class CustomerPresenter extends SecuredPresenter
{
    public $customerId;

    /** @var \App\Forms\CustomerForm @inject */
    public $customerForm;

    /** @var \App\Model\CustomerRepository @inject */
    public $customerRepository;

    /** @var \App\Model\AddressRepository @inject */
    public $addressRepository;

    public function renderDefault()
    {
        $this->session->getSection('customer')->remove();
        $this->template->customers = $this->customerRepository->getAllCustomers();
    }

    public function createComponentCustomerForm()
    {
        $form = $this->customerForm->create($this->customerId, $this);
        return $form;
    }

    public function handleCheckAres($ico)
    {
        $ares_url = 'http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico=';
        $ch = curl_init($ares_url . $ico);
        if ($ch) {
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $content = curl_exec($ch);
            curl_close($ch);
            $xml = @simplexml_load_string($content);
        }

        $company = [];
        if (($xml)) {
            $ns = $xml->getDocNamespaces();
            $data = $xml->children($ns['are']);
            $el = $data->children($ns['D'])->VBAS;
            if (strval($el->ICO) == $ico && $el) {
                $company['ico'] = strval($el->ICO);
                $company['dic'] = strval($el->DIC);
                $company['name'] = strval($el->OF);
                $company['street'] = strval($el->AA->NU) . ' ' . strval($el->AA->CD) . '/' . strval($el->AA->CO);
                $company['city'] = strval($el->AA->N) . '-' . strval($el->AA->NCO);
                $company['zip'] = strval($el->AA->PSC);
                $company['country'] = strval($el->AA->NS);
                $company['state'] = 'OK';
            } else {
                $company['state'] = 'IČ firmy nebylo nalezeno';
            }
        } else {
            $company['state'] = 'Databáze ARES není dostupná';
        }

        $this->payload->company = $company;
        $this->payload->ico = $ico;
        $this->sendPayload();
    }

    public function actionEdit($id)
    {
        $this->customerId = $id;

        if ($id) {
            $customer = $this->customerRepository->get($id);
            $address = $this->addressRepository->get($customer->address);
            $merge = Arrays::mergeTree($customer->toArray(), $address->toArray());
            $this['customerForm']->setDefaults($merge);
            $this['customerForm']['address']->setDefaults($customer->ref('address_id'));
            $this->session->getSection('customer')->values = $merge;
        } else {
            $this->error('Zákazník nebyl nalezen.');
        }
    }
}
