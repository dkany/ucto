<?php

namespace App\Model;

class BusinessCaseCategoryRepository extends Repository
{

    protected $table = 'business_case_category';

    public function fetchPairs()
    {
        return $this->database->table($this->table)->order('id')->fetchPairs('id', 'name');
    }

    public function fetch($id)
    {
        return $this->database->table($this->table)->where('id',$id)->fetch();
    }
    
    public function getBusinessCaseCategory($id)
    {
        return $this->database->table($this->table)->where('id', $id)->fetch();
    }
    
    
}
