<?php

namespace App\Model;

use Nette;
use Nette\Database\Context;
use Nette\SmartObject;

abstract class Repository
{

    use SmartObject;

    /** @var Nette\Database\Context */
    protected $database;

    /** @var Table of current repository */
    protected $table;

    public function __construct(Context $database)
    {
        $this->database = $database;
    }

    protected function fetch($id)
    {
    }

    protected function get($id)
    {
    }

    protected function insert($data)
    {
    }

    protected function delete($id)
    {
    }
}
