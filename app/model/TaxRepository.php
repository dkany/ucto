<?php

namespace App\Model;

class TaxRepository extends Repository
{

    protected $table = 'taxes';

    public function fetchPairs()
    {
        return $this->database->table($this->table)->select('id, vat')->fetchPairs('id', 'id');
    }
    
    public function get($id)
    {
        return $this->database->table($this->table)->get($id);
    }
}
