<?php

namespace App\Model;

class CurrencyRepository extends Repository
{

    protected $table = 'currency';

    public function fetch($id)
    {
        return $this->database->table($this->table)->where('id', $id)->fetch();
    }

    public function fetchPairs()
    {
        return $this->database->table($this->table)->order('name')->fetchPairs('id', 'name');
    }
}
