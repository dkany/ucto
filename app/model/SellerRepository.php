<?php

namespace App\Model;

class SellerRepository extends Repository
{

    protected $table = 'users';

    public function getSellers()
    {
        return $this->database->table($this->table)->where('role', 'seller')->fetchPairs('id', 'name');
    }

    public function getSeller($id)
    {
        return $this->database->table($this->table)->where('role', 'seller')->where('id', $id)->fetch();
    }
}
