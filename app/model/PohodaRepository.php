<?php

namespace App\Model;

class PohodaRepository extends Repository 
{
    protected $table = 'log_pohoda';
    
    public function insertOrderData($data, $userId, $businessCaseId)
    {
	return $this->database->table($this->table)->insert([
	    'id' => $data['id'],
	    'xml' => $data['template'],
	    'type' => 'order_send',
	    'user_id' => $userId,
	    'business_case_id' => $businessCaseId
	]);
    }
    
    public function insertCustomerDate($data, $userId)
    {
	return $this->database->table($this->table)->insert([
	    'id' => $data['id'],
	    'xml' => $data['template'],
	    'type' => 'address_send',
	    'user_id' => $userId
	]);
    }
}

