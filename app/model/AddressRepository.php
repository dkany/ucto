<?php

namespace App\Model;

class AddressRepository extends Repository
{
    
    protected $table = 'address';

    public function insert($data)
    {
        return $this->database->table($this->table)->insert($data);
    }

    public function update($id, $data)
    {
        return $this->database->table($this->table)->where('id', $id)->update($data);
    }

    public function get($id)
    {
        return $this->database->table($this->table)->get($id);
    }
}
