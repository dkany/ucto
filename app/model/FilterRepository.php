<?php

namespace App\Model;

class FilterReceivedInvoiceRepository extends Repository
{

    protected $table = 'received_invoices';

    public function filterByYear($year)
    {
        return $this->database->table($this->table)->where('year', $year);
    }
}
