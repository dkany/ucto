<?php

namespace App\Model;

class ReceivedInvoiceRepository extends Repository
{

    protected $table = 'received_invoices';

    public function get($id)
    {
        return $this->database->table($this->table)->get($id);
    }

    public function getAll()
    {
        return $this->database->table($this->table)->where('deleted', false);
    }

    public function fetch($id)
    {
        return $this->database->table($this->table)->where('id', $id)->fetch();
    }

    public function insert($data)
    {
        return $this->database->table($this->table)->insert($data);
    }

    public function update($id, $data)
    {
        return $this->database->table($this->table)->where('id', $id)->update($data);
    }

    public function delete($id)
    {
        return $this->database->table($this->table)->where('id', $id)->delete();
    }

    public function command($id)
    {
        return $this->database->table($this->table)->where('id', $id)->update([
            'prepare_to_pay' => date('Y-m-d'),
            'state' => 'Příkaz'
        ]);
    }
}
