<?php

namespace App\Model;

class PaidInvoiceRepository extends Repository
{

    protected $table = 'received_invoices';

    public function getInvoicesToPay()
    {
        return $this->database->table($this->table)->where('prepare_to_pay IS NOT NULL')->where('paid IS NULL');
    }

    public function paid($id)
    {
        return $this->database->table($this->table)->where('id', $id)->update([
            'paid' => date('Y-m-d'),
            'state' => 'Uhrazeno'
        ]);
    }
}
