<?php

namespace App\Model;

class BusinessCaseItemRepository extends Repository
{

    protected $table = 'business_case_item';

    public function insertItem($caseId, $data)
    {
        return $this->database->table($this->table)->insert([
            'business_case_id' => $caseId,
            'name' => $data->name,
            'price' => $data->price,
            'amount' => $data->amount,
            'taxes_id' => $data->taxes_id
        ]);
    }

    public function update($itemId, $data)
    {
        return $this->database->table($this->table)->where('id', $itemId)->update([
            'name' => $data->name,
            'price' => $data->price,
            'amount' => $data->amount,
            'taxes_id' => $data->taxes_id
        ]);
    }

    public function get($businessCaseId)
    {
        return $this->database->table($this->table)->where('business_case_id', $businessCaseId);
    }

    public function getAll()
    {
        return $this->database->table($this->table);
    }

    public function deleteItem($businessItemId)
    {
        return $this->database->table($this->table)->where('id', $businessItemId)->delete();
    }

    public function deleteItemsCase($businessItemId)
    {
        return $this->database->table($this->table)->where('business_case_id', $businessItemId)->delete();
    }

}

