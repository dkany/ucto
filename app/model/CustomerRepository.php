<?php

namespace App\Model;

class CustomerRepository extends Repository
{

    protected $table = 'customers';

    public function getAllCustomers()
    {
        return $this->database->table($this->table);
    }

    public function get($id)
    {
        return $this->database->table($this->table)->get($id);
    }

    public function fetchPairs()
    {
        return $this->database->table($this->table)->select('id, name')->order('name')->fetchPairs('id', 'name');
    }

    public function fetch($id)
    {
        return $this->database->table($this->table)->where('id', $id)->fetch();
    }

    public function insert($data)
    {
        return $this->database->table($this->table)->insert($data);
    }

    public function update($id, $data)
    {
        return $this->database->table($this->table)->where('id', $id)->update($data);
    }

    public function getAdrress($id)
    {
        $row = $this->database->table($this->table)->get($id);
        return $row->ref('address_id');
    }
}
