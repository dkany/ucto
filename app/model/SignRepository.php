<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;
use Nette\Database\Context;

class SignRepository implements Nette\Security\IAuthenticator
{

    use Nette\SmartObject;

    /** @var Nette\Database\Context */
    private $database;


    public function __construct(Context $database)
    {
        $this->database = $database;
    }


    public function authenticate(array $credentials): Nette\Security\IIdentity
    {
        list($username, $password) = $credentials;

        $row = $this->database->table('users')->where('name', $username)->fetch();

        if (!$row) {
            throw new Nette\Security\AuthenticationException('Přihlašovací jméno je chybné.', self::IDENTITY_NOT_FOUND);
        } elseif (!Passwords::verify($password, $row['password'])) {
            throw new Nette\Security\AuthenticationException('Heslo je špatné.', self::INVALID_CREDENTIAL);
        } elseif (Passwords::needsRehash($row['password'])) {
            $row->update([
                'password' => Passwords::hash($password),
            ]);
        }

        $arr = $row->toArray();
        unset($arr['password']);
        return new Nette\Security\Identity($row['id'], $row['role'], $arr);
    }

    public function add(string $username, string $email, string $password): void
    {
        try {
            $this->database->table('users')->insert([
                'name' => $username,
                'password' => Passwords::hash($password),
                'email' => $email,
                'role' => 'guest'
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            throw new DuplicateNameException;
        }
    }
}
