<?php

namespace App\Model;

class UserRepository extends Repository
{

    protected $table = 'users';

    public function fetch($id)
    {
        return $this->database->table($this->table)->where('id', $id)->fetch();
    }

    public function fetchPairs()
    {
        return $this->database->table($this->table)->select('id, name')->order('name')->fetchPairs('id', 'name');
    }
}
