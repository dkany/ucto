<?php

namespace App\Model;

class ApprovalInvoiceRepository extends Repository
{

    protected $table = 'received_invoices';

    public function getInvoicesToSign($id)
    {
        return $this->database->table($this->table)->where('approve_user_id ? AND reject ?', $id, null)->where('approved IS NULL');
    }

    public function approve($id)
    {
        return $this->database->table($this->table)->where('id', $id)->update([
            'approved' => date('Y-m-d'),
            'state' => 'Schváleno'
        ]);
    }
    
    public function reject($id, $note)
    {
	return $this->database->table($this->table)->where('id', $id)->update([
	   'reject' => date('Y-m-d'),
	   'reject-note' => $note,
	   'state' => 'Odmítnuto'
	]);
    }
    
    public function changeOfApproval($id, $newUserId)
    {
	return $this->database->table($this->table)->where('id', $id)->update(['approve_user_id' => $newUserId]);
    }
}
