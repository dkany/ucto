<?php

namespace App\Model;

class BusinessCaseRepository extends Repository
{

    protected $table = 'business_case';

    public function insert($data)
    {
        return $this->database->table($this->table)->insert([
            'name' => $data->name,
            'costs' => $data->costs,
            'date_created' => $data->date_created,
            'due_date' => $data->due_date,
            'user_id' => $data->user_id,
            'order_number' => $data->order_number,
            'note' => $data->note,
            'note_to_invoice' => $data->note_to_invoice,
            'storno' => $data->storno,
            'internal_exhibit' => $data->internal_exhibit,
            'cost_exhibit' => $data->cost_exhibit,
            'business_case_category_id' => $data->business_case_category_id,
            'customer_id' => $data->customer_id,
            'currency_id' => $data->currency_id,
            'attachment' => $data->attachment instanceof \Nette\Http\FileUpload ? $data->attachment->getSanitizedName() : null
        ]);
    }

    public function update($id, $data)
    {
        return $this->database->table($this->table)->where('id', $id)->update([
            'name' => $data->name,
            'costs' => $data->costs,
            'date_created' => $data->date_created,
            'due_date' => $data->due_date,
            'user_id' => $data->user_id,
            'order_number' => $data->order_number,
            'note' => $data->note,
            'note_to_invoice' => $data->note_to_invoice,
            'storno' => $data->storno,
            'internal_exhibit' => $data->internal_exhibit,
            'cost_exhibit' => $data->cost_exhibit,
            'business_case_category_id' => $data->business_case_category_id,
            'customer_id' => $data->customer_id,
            'currency_id' => $data->currency_id,
            'attachment' => $data->attachment
        ]);
    }

    public function getAll()
    {
        return $this->database->table($this->table);
    }

    public function get($id)
    {
        return $this->database->table($this->table)->get($id);
    }
}
