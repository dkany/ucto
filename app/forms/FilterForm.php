<?php

namespace App\Forms;

use Nette;
use App\Model\FilterReceivedInvoiceRepository;

class FilterForm
{

    use Nette\SmartObject;

    /** @var \App\Forms\FormFactory */
    public $formFactory;

    /** @var \App\Model\FilterReceivedInvoiceRepository */
    public $filterReceivedInvoiceRepository;


    public function __construct(FormFactory $formFactory, FilterReceivedInvoiceRepository $filterReceivedInvoiceRepository)
    {
        $this->formFactory = $formFactory;
        $this->filterReceivedInvoiceRepository = $filterReceivedInvoiceRepository;
    }

    public function create()
    {
        $form = $this->formFactory->create();

        $year = [];
        for ($i = 2017; $i <= date('Y', time()); $i++) {
            $year[$i] = $i;
        }

        $form->addSelect('year', 'Rok', $year)
            ->setDefaultValue(date('Y', time()));

        $form->addSubmit('send', 'Filtrovat');

        $form->addProtection();

        $form->onSuccess[] = [$this, 'applyFilters'];

        return $form;
    }

    public function applyFilters($year = null)
    {
        return $this->filterReceivedInvoiceRepository->filterByYear($year);
    }
}
