<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;

class FormFactory
{
    use Nette\SmartObject;

    public function create()
    {
        $form = new Form;
        $form->onRender[] = [$this, 'makeBootstrap3'];
        return $form;
    }

    public function makeBootstrap3(Form $form)
    {
        $renderer = $form->getRenderer();
        $renderer->wrappers['controls']['container'] = null;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container'] = 'div class=col-sm-9';
        $renderer->wrappers['label']['container'] = 'div class="col-sm-3 control-label"';
        $renderer->wrappers['control']['description'] = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
        $form->getElementPrototype()->class('form-horizontal');
        foreach ($form->getControls() as $control) {
            $type = $control->getOption('type');
            if ($type === 'button') {
                $control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
                $usedPrimary = true;
            } elseif (in_array($type, ['text', 'textarea', 'select'], true)) {
                $control->getControlPrototype()->addClass('form-control');
            } elseif (in_array($type, ['checkbox', 'radio'], true)) {
                $control->getSeparatorPrototype()->setName('div')->addClass($type);
            }
        }
    }
}
