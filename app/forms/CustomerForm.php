<?php

namespace App\Forms;

use Nette;
use App\Model\SellerRepository;
use App\Model\AddressRepository;
use App\Model\CustomerRepository;
use Nette\Http\Session;

class CustomerForm
{
    
    use Nette\SmartObject;
   
    /*** @var ID of customer */
    private $customerId;

    /** @var CustomerPresenter */
    private $presenter;

    /** @var \App\Forms\FormFactory */
    public $formFactory;

    /** @var  \Nette\Http\Session */
    private $session;

    /** @var \App\Model\CustomerRepository */
    public $customerRepository;

    /** @var \App\Model\SellerRepository */
    public $sellerRepository;

    /** @var \App\Model\AddressRepository */
    public $addressRepository;

    public function __construct(Session $session, FormFactory $formFactory, CustomerRepository $customerRepository, SellerRepository $sellerRepository, AddressRepository $addressRepository)
    {
        $this->formFactory = $formFactory;
        $this->session = $session;
        $this->customerRepository = $customerRepository;
        $this->sellerRepository = $sellerRepository;
        $this->addressRepository = $addressRepository;
    }

    public function create($id, $presenter)
    {
        $this->customerId = $id;

        $this->presenter = $presenter;

        $form = $this->formFactory->create();

        $form->addGroup('Zákaznické údaje');

        $form->addMultiSelect('user_id', 'Obchodník:', $this->sellerRepository->getSellers())
            ->setAttribute('id', 'js-search-select2')
            ->setRequired('Prosím doplňte obchodníka');

        $form->addText('name', 'Název firmy (interní):')
            ->addRule($form::MAX_LENGTH, null, 50)
            ->setRequired('Prosím doplňte %label');

        $form->addInteger('ico', 'IČO:')
            ->setRequired('Prosím doplňte %label');

        $form->addText('dic', 'DIČ:')
            ->addRule($form::MAX_LENGTH, null, 50)
            ->setRequired('Prosím doplňte %label');

        $form->addButton('ares', 'Získat data z ARESU')
            ->setHtmlId('frm-customerForm-ares')
            ->setAttribute('class', 'pull-right')
            ->setOmitted();


        $form->addGroup('Adresa');
        $address = $form->addContainer('address');

        $address->addText('name', 'Název společnosti:')
            ->addRule($form::MAX_LENGTH, null, 100)
            ->setRequired();

        $address->addText('street', 'Ulice:')
            ->addRule($form::MAX_LENGTH, null, 50)
            ->setRequired();

        $address->addText('city', 'Město:')
            ->addRule($form::MAX_LENGTH, null, 50)
            ->setRequired();

        $address->addInteger('zip', 'PSČ:')
            ->addRule($form::MAX_LENGTH, null, 15)
            ->setRequired();

        $address->addText('country', 'Stát:')
            ->addRule($form::MAX_LENGTH, null, 50)
            ->setRequired();


//        $form->addCheckbox('anotherAddress', 'Jiná fakturační adresa')
//            ->addCondition($form::EQUAL, true)
//            ->toggle('billing-address');
//
//
//        $form->addGroup('Fakturační adresa')->setOption('id', 'billing-address');
//        $billing_adress = $form->addContainer('billing_address');
//
//        $billing_adress->addText('name', 'Název společnosti:')
//            ->addRule($form::MAX_LENGTH, null, 100)
//            ->setRequired(false);
//
//        $billing_adress->addText('street', 'Ulice:')
//            ->addRule($form::MAX_LENGTH, null, 50)
//            ->setRequired(false);
//
//        $billing_adress->addText('city', 'Město:')
//            ->addRule($form::MAX_LENGTH, null, 50)
//            ->setRequired(false);
//
//        $billing_adress->addInteger('zip', 'PSČ:')
//            ->addRule($form::MAX_LENGTH, null, 15)
//            ->setRequired(false);
//
//        $billing_adress->addText('country', 'Stát:')
//            ->addRule($form::MAX_LENGTH, null, 50)
//            ->setRequired(false);


        $form->addGroup('Kontaktní osoba');
        $form->addText('contact_person', 'Kontaktní osoba:')
            ->setRequired(false);

        $form->addInteger('phone', 'Telefon:')
            ->setRequired(false);

        $form->addEmail('email', 'Email:')
            ->addRule($form::MAX_LENGTH, null, 50)
            ->setRequired(false);

        $form->addText('www', 'Webové stránky:')
            ->addRule($form::MAX_LENGTH, null, 50)
            ->setRequired(false);

        $form->addTextArea('note', 'Poznámky:')
            ->addRule($form::MAX_LENGTH, null, 50)
            ->setRequired(false);

        $form->addSubmit('save', 'Uložit zákazníka');

        $form->addProtection();

        $form->onSuccess[] = [$this, 'processForm'];

        return $form;
    }

    public function processForm($form, $values)
    {
        if ($this->customerId) {
            $address_id = $this->session->getSection('customer')->values['address_id'];
            $this->addressRepository->update($address_id, $values->address);
            unset($values->address);
            $this->customerRepository->update($this->customerId, $values);
            $this->presenter->getPresenter()->redirect('Pohoda:sendCustomerToPohoda', $this->customerId);
        } else {
            $address_id = $this->addressRepository->insert($values->address);
            unset($values->address);
            $values->address_id = $address_id;
            $customer = $this->customerRepository->insert($values);
            $this->presenter->getPresenter()->redirect('Pohoda:sendCustomerToPohoda', $customer->id);
        }
    }
}

