<?php

namespace App\Forms;

use Nette;
use Nette\Security\User;
use Nette\Http\Session;
use App\Model\ReceivedInvoiceRepository;
use App\Model\UserRepository;
use App\Model\CustomerRepository;
use App\Model\CurrencyRepository;
use App\Libraries\Formater;
use App\Libraries\FileManager;

class InvoiceForm
{
    use Nette\SmartObject;

    /** @var \App\Forms\FormFactory */
    public $formFactory;

    /** @var \App\Forms\FormFactory */
    public $formater;

    /** @var \App\Libraries\FileManager */
    public $fileManager;

    /** @var Nette\Security\User */
    public $user;

    /** @var  Nette\Http\Session */
    private $session;

    /** @var ID of Received Invoice */
    private $id;

    /** @var receivedInvoice */
    private $receivedInvoiceRepository;

    /** @var ID of Received Invoice */
    private $userRepository;

    /** @var \App\Model\CustomerRepository */
    private $customerRepository;

    /** @var \App\Model\CurrencyRepository */
    private $currencyRepository;

    public function __construct(User $user, Session $session, FormFactory $formFactory, ReceivedInvoiceRepository $receivedInvoiceRepository, UserRepository $userRepository, CustomerRepository $customerRepository, CurrencyRepository $currencyRepository, Formater $formater, FileManager $fileManager)
    {
        $this->session = $session;
        $this->user = $user;
        $this->formFactory = $formFactory;
        $this->receivedInvoiceRepository = $receivedInvoiceRepository;
        $this->userRepository = $userRepository;
        $this->customerRepository = $customerRepository;
        $this->currencyRepository = $currencyRepository;
        $this->formater = $formater;
        $this->fileManager = $fileManager;
    }

    public function create($id)
    {
        $this->id = $id;

        $form = $this->formFactory->create();

        $year = [];

        for ($i = 2017; $i <= date('Y', time()); $i++) {
            $year[$i] = $i;
        }

        $form->addSelect('year', 'Rok', $year)
            ->setDefaultValue(date("Y", time()));

        $form->addInteger('number', 'Pořadové číslo')
            ->setEmptyValue('Při nevyplnění bude doplněno automaticky');

        $form->addSelect('customer_id', 'Zákazník', $this->customerRepository->fetchPairs())
            ->setPrompt('Vyberte zákazníka')
            ->setAttribute('id', 'js-search-select2')
            ->setAttribute('class', 'customer_id')
            ->setRequired('%label nebyl vybrán');

//	Tlačítko na vytvoření zákazníka(zatím nedodělané)	
//	$form->addButton('new_customer','Nový zákazník')
//	    ->setAttribute('class','right pull-right')
//	    ->setOmitted();

        $form->addText('receive_date', 'Datum přijetí faktury')
            ->setType('date')
            ->setDefaultValue(date_format(new \DateTime(), 'd-m-Y'))
            ->setRequired('%label nebylo zadáno');

        $form->addInteger('invoice_number', 'Číslo faktury')
            ->addRule($form::MAX_LENGTH, '%label smí být dlouhé maximálně %d', 15)
            ->setRequired('%label nebylo vyplněno');

        $form->addInteger('variable_symbol', 'Variabilní symbol')
            ->setRequired(false)
            ->addRule($form::MAX_LENGTH, '%label může být dlouhý maximálně %d', 10)
            ->addCondition($form::FILLED)
            ->addRule($form::PATTERN, '%label musím obsahovat pouze číslice', '^\d+$');

        $form->addUpload('upload', 'Soubor s fakturou')
            ->addRule($form::MIME_TYPE, '%label musí být ve formátu PDF.', ['application/pdf'])
            ->setRequired('%label nebyl vybrán');

        $form->addText('due_date', 'Datum splatnosti')
            ->setType('date')
            ->setRequired('%label nebylo zadáno');

        $form->addTextArea('subject', 'Předmět fakturace');

        $form->addInteger('amount', 'Částka na faktuře')
            ->addRule($form::PATTERN, '%label musím obsahovat pouze číslice (případně čárku)', '^-?\d+[.,]?\d+$')
            ->setRequired('%label nebyla vyplněna');

        $form->addSelect('currency_id', 'Měna', $this->currencyRepository->fetchPairs())
            ->setPrompt('Vyberte měnu')
            ->setRequired('%label nebyla vybrána');

        $form->addMultiSelect('approve_user_id', 'Schvalující lidé', $this->userRepository->fetchPairs())
            ->setRequired('Nebyl vybrán nikdo na schválení');

        $form->addSubmit('send', 'Uložit');

        $form->addProtection();

        $form->onSuccess[] = [$this, 'processForm'];

        return $form;
    }

    public function processForm($form, $values)
    {
        $this->fileManager->uploadReceivedInvoice($values->upload, $values->invoice_number, $form);

        if ($values->upload->isOk()) {
            $values->upload = $this->formater->formatUrl($values->upload);
        } else {
            $values->upload = $form['upload']->getOption('description');
        }

        if ($this->id) {
            $values->state = $this->receivedInvoiceRepository->fetch($this->id)->state;
        }

        $values->created_user_id = $this->user->getId();
        $this->session->getSection('summaryInvoice')->values = $values;
    }
}
