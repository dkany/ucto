<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use App\Model\SignRepository;

class SignUpForm
{

    use Nette\SmartObject;

    /** @var \App\Forms\FormFactory */
    public $formFactory;

    /** @var \App\Model\SignRepository */
    public $signRepository;

    public function __construct(FormFactory $formFactory, SignRepository $signRepository)
    {
        $this->formFactory = $formFactory;
        $this->signRepository = $signRepository;
    }

    public function create(callable $onSuccess)
    {
        $form = $this->formFactory->create();

        $form->addText('username', 'Uživatelské jméno: ')
            ->setRequired('Zadejte vaše uživatelské jméno');

        $form->addEmail('email', 'E-mail :')
            ->setRequired('Zadejte váš e-mail');

        $form->addPassword('password', 'Heslo: ')
            ->setRequired('Zadejte vaše heslo');

        $form->addSubmit('send', 'Zaregistrovat se');

        $form->addProtection();

        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
            try {
                $this->signRepository->add($values->username, $values->email, $values->password);
            } catch (Nette\Application\BadRequestException $e) {
                throw new Nette\Security\AuthenticationException('Heslo nebo uživatelské jméno je nesprávné');
            }
            $onSuccess();
        };
        return $form;
    }
}
