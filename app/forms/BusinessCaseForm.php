<?php

namespace App\Forms;

use Nette;
use Nette\Forms\Container;
use Nette\Forms\Controls\SubmitButton;
use Nette\Application\UI\Form;
use Nette\Http\Session;
use App\Model\BusinessCaseRepository;
use App\Model\BusinessCaseItemRepository;
use App\Model\BusinessCaseCategoryRepository;
use App\Model\SellerRepository;
use App\Model\CustomerRepository;
use App\Model\CurrencyRepository;
use App\Model\TaxRepository;

class BusinessCaseForm
{
    use Nette\SmartObject;

    private $id;
    private $storno;
    private $cost_exhibit;
    private $presenter;

    /** @var \App\Forms\FormFactory */
    public $formFactory;

    /** @var \Nette\Http\Session */
    public $session;

    /** @var \App\Model\BusinessCaseCategoryRepository */
    public $businessCaseCategoryRepository;

    /** @var \App\Model\BusinessCaseRepository */
    public $businessCaseRepository;

    /** @var \App\Model\BusinessCaseItemRepository */
    public $businessCaseItemRepository;

    /** @var \App\Model\SellerRepository */
    public $sellerRepository;

    /** @var \App\Model\CustomerRepository */
    public $customerRepository;

    /** @var \App\Model\CurrencyRepository */
    public $currencyRepository;

    /** @var \App\Model\TaxRepository */
    public $taxRepository;

    public function __construct(FormFactory $formFactory, Session $session, BusinessCaseRepository $businessCaseRepository, BusinessCaseItemRepository $businessCaseItemRepository, BusinessCaseCategoryRepository $businessCaseCategoryRepository, SellerRepository $sellerRepository, CustomerRepository $customerRepository, CurrencyRepository $currencyRepository, TaxRepository $taxRepository)
    {
        $this->session = $session;
        $this->formFactory = $formFactory;
        $this->businessCaseCategoryRepository = $businessCaseCategoryRepository;
        $this->businessCaseRepository = $businessCaseRepository;
        $this->businessCaseItemRepository = $businessCaseItemRepository;
        $this->sellerRepository = $sellerRepository;
        $this->customerRepository = $customerRepository;
        $this->currencyRepository = $currencyRepository;
        $this->taxRepository = $taxRepository;
    }

    public function create($id, $presenter)
    {

        $this->id = $id;

        $this->presenter = $presenter;

        $form = $this->formFactory->create();

        $removeEvent = [$this, 'businessCaseFormItemRemove'];

        $form->addSelect('business_case_category_id', 'Kategorie', $this->businessCaseCategoryRepository->fetchPairs())
            ->setPrompt('Vyberte kategorii')
            ->setRequired('%label nebyla vybrána.');

        $form->addSelect('user_id', 'Obchodník', $this->sellerRepository->getSellers())
            ->setPrompt('Vyberte obchodníka')
            ->setRequired('%label nebyl vybrána.');

        $form->addText('name', 'Jméno obchodního případu')
            ->setRequired('%label nebylo zadáno.');

        $form->addText('date_created', 'Datum založení případu')
            ->setDefaultValue(date('Y-m-d'))
            ->setRequired('%label nebylo zadáno.')
            ->setType('date');

        $form->addSelect('customer_id', 'Zákazník', $this->customerRepository->fetchPairs())
            ->setAttribute('id', 'js-search-select2')
            ->setPrompt('Vyberte zákazníka')
            ->setRequired('%label nebyl vybrán.');

        $form->addInteger('order_number', 'Číslo objednávky')
            ->addRule($form::MAX_LENGTH, '%label smí být dlouhé maximálně %d', 20);

        $form->addSelect('currency_id', 'Měna', $this->currencyRepository->fetchPairs())
            ->setPrompt('Vyberte měnu')
            ->setRequired('%label nebyla vybrána.');

        $form->addText('due_date', 'Datum splatnosti faktury')
            ->setDefaultValue(date_format(new \DateTime('+30day'), 'Y-m-d'))
            ->setRequired('Zadejte datum splatnost faktury.')
            ->setType('date');

        $form->addText('bill_id', 'Číslo faktury')
            ->setAttribute('readonly')
            ->setOmitted();

        $form->addText('date_paid', 'Datum zaplacení')
            ->setAttribute('readonly')
            ->setType('date')
            ->setOmitted();

        $form->addText('date_invoice', 'Datum fakturace')
            ->setAttribute('readonly')
            ->setType('date')
            ->setOmitted();

        $form->addInteger('costs', 'Náklady (interní položka)');

        $form->addTextArea('note', 'Poznámka (interní)');

        $form->addTextArea('note_to_invoice', 'Text na fakturu - může být dlouhý text');

        $form->addUpload('attachment', 'Příloha (interní)');

        $form->addCheckbox('storno', 'Storno obchodního případu');
        $this->storno = $form['storno'];

        $form->addCheckbox('cost_exhibit', 'Nákladový exhibit')
            ->addConditionOn($form['storno'], $form::EQUAL, true)
            ->addRule($form::EQUAL, '%label nemůže být použit spolu se stornem.', false)
            ->setRequired(false);
        $this->cost_exhibit = $form['cost_exhibit'];

        $form->addCheckbox('internal_exhibit', 'Interní exhibit');

        $businessCaseItems = $form->addDynamic('businessCaseItems', function (Container $businessCaseItem) use ($removeEvent) {

            $businessCaseItem->addHidden('id');

            $businessCaseItem->addText('name', 'Jméno položky')
                ->setAttribute('class', 'form-control')
                ->addConditionOn($this->cost_exhibit, Form::EQUAL, false)
                ->setRequired('%label nebylo vylněno');

            $businessCaseItem->addInteger('price', 'Cena bez daně')
                ->setAttribute('class', 'form-control')
                ->addConditionOn($this->cost_exhibit, Form::EQUAL, false)
                ->setRequired('%label není vylněna');

            $businessCaseItem->addInteger('amount', 'Množství')
                ->setAttribute('class', 'form-control')
                ->addConditionOn($this->cost_exhibit, Form::EQUAL, false)
                ->setRequired('%label není vylněno');

            $businessCaseItem->addSelect('taxes_id', 'Sazba daně', $this->taxRepository->fetchPairs())
                ->setAttribute('class', 'form-control')
                ->setValue('21%')
                ->addConditionOn($this->cost_exhibit, Form::EQUAL, false)
                ->setRequired('Nebyla vybrána sazba daně');

            $businessCaseItem->addSubmit('remove', 'Odstranit položku')
                ->setAttribute('class', 'btn btn-default')
                ->setValidationScope(false)
                ->onClick[] = $removeEvent;
        }, 1);

        $businessCaseItems->addSubmit('addItem', 'Přidat položku')
            ->setValidationScope(false)
            ->onClick[] = [$this, 'businessCaseFromItemAdd'];

        $form->addSubmit('send', 'Uložit obchodní případ')
            ->setAttribute('class', 'btn btn-primary btn-main pull-right')
            ->onClick[] = [$this, 'processForm'];

        $form->addProtection();

        return $form;
    }

    public function processForm(SubmitButton $button)
    {
        $values = $button->form->values;

        if ($values->attachment->isOk()) {
            $path = __DIR__ . '/../../temp/attachments/' . $values->attachment->getSanitizedName();
            $values->attachment->move($path);
        } else {
            $values->attachment = $button->form['attachment']->getOption('description');
        }

        if ($values->cost_exhibit == 1) {
            unset($values->businessCaseItems);
            $this->session->getSection('summaryBusinessCase')->values = $values;
        } elseif ($values->storno == 1) {
            foreach ($values->businessCaseItems as $id => $item) {
                $values['businessCaseItems'][$id]['price'] = $item->price * (-1);
            }
            $this->session->getSection('summaryBusinessCase')->values = $values;
            $this->session->getSection('summaryBusinessCaseItem')->values = $values->businessCaseItems;
        } else {
            $this->session->getSection('summaryBusinessCaseItem')->values = $values->businessCaseItems;
            unset($values->businessCaseItems);
            $this->session->getSection('summaryBusinessCase')->values = $values;
        }

        $this->presenter->getPresenter()->redirect('Business:summary', $this->id);
    }

    public function businessCaseFromItemAdd(SubmitButton $button)
    {
        $businessCaseItem = $button->parent;

        if ($businessCaseItem->isAllFilled()) {
            $button->parent->createOne();
        }
    }

    public function businessCaseFormItemRemove(SubmitButton $button)
    {
        $businessCaseItem = $button->parent->parent;
        $businessCaseItem->remove($button->parent, true);
    }
}
