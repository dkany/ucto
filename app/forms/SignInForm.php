<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;

class SignInForm
{

    use Nette\SmartObject;

    /** @var \App\Forms\FormFactory */
    public $formFactory;

    /** @var Nette\Security\User */
    public $user;

    public function __construct(User $user, FormFactory $formFactory)
    {
        $this->user = $user;
        $this->formFactory = $formFactory;
    }

    public function create(callable $onSuccess)
    {
        $form = $this->formFactory->create();
        $form->addText('username', 'Uživatelské jméno: ')
            ->setRequired('Zadejte vaše uživatelské jméno');

        $form->addPassword('password', 'Heslo: ')
            ->setRequired('Zadejte vaše heslo');

        $form->addSubmit('send', 'Přihlásit se');

        $form->addProtection();

        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
            try {
                $this->user->login($values->username, $values->password);
            } catch (Nette\Application\BadRequestException $e) {
                throw new Nette\Security\AuthenticationException('Heslo nebo uživatelské jméno je nesprávné');
            }
            $onSuccess();
        };
        return $form;
    }
}
