$(document).ready(function() {
    $('select[multiple]').select2();
    $('#js-search-select2').select2();
    
});

Nette.toggle = function (id, visible) {
    var el = $('#' + id);
    if (visible) {
        el.slideDown();
    } else {
        el.slideUp();
    }
};

