$(document).ready(function() {
    $('#data-table').DataTable({
	"language": {
	    sProcessing: "Provádím...",
	    sLengthMenu: "Zobraz záznamů _MENU_",
	    sZeroRecords: "Žádné záznamy nebyly nalezeny",
	    sInfo: "Zobrazuji _START_ až _END_ z celkem _TOTAL_ záznamů",
	    sInfoEmpty: "Zobrazuji 0 až 0 z 0 záznamů",
	    sInfoFiltered: "(filtrováno z celkem _MAX_ záznamů)",
	    sInfoPostFix: "",
	    sSearch: "Hledat:",
	    sUrl: "",
	    oPaginate: {
		sFirst: "První",
		sPrevious: "Předchozí",
		sNext: "Další",
		sLast: "Poslední"
	    }
	},
	paging: true,
	bFilter: true,
	ordering: true,
	searching: true
    });

    $('.table.dataTable thead th, table.dataTable thead td').css({'border':'1px solid #ddd','padding':'5px'});
});

