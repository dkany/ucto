-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `ucto`;
CREATE DATABASE `ucto` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `ucto`;

DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `street` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `zip` varchar(15) NOT NULL,
  `country` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `address` (`id`, `name`, `street`, `city`, `zip`, `country`) VALUES
(13,	'Alza.cz a.s.',	'Jateční 1530/33',	'Praha-Holešovice',	'17000',	'Česká republika'),
(14,	'Anywhere s.r.o.',	'Jaselská 275/6',	'Praha-Bubeneč',	'16000',	'Česká republika');

DROP TABLE IF EXISTS `business_case`;
CREATE TABLE `business_case` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `costs` int(11) DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `date_paid` date DEFAULT NULL,
  `date_invoice` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `paid` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `order_number` varchar(50) NOT NULL,
  `log_pohoda_id` varchar(25) DEFAULT NULL,
  `log_pohoda_xml_type` varchar(80) DEFAULT NULL,
  `note` text,
  `note_to_invoice` text,
  `storno` int(11) NOT NULL DEFAULT '0',
  `internal_exhibit` int(1) NOT NULL DEFAULT '0',
  `cost_exhibit` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `business_case_category_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `currency_id` int(3) NOT NULL,
  `bill_id` int(11) DEFAULT NULL,
  `attachment` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `business_case_category_id` (`business_case_category_id`),
  KEY `user_id` (`user_id`),
  KEY `customer_id` (`customer_id`),
  KEY `currency_id` (`currency_id`),
  CONSTRAINT `business_case_ibfk_2` FOREIGN KEY (`business_case_category_id`) REFERENCES `business_case_category` (`id`) ON DELETE NO ACTION,
  CONSTRAINT `business_case_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION,
  CONSTRAINT `business_case_ibfk_6` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `business_case_ibfk_7` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `business_case` (`id`, `name`, `costs`, `date_created`, `date_paid`, `date_invoice`, `due_date`, `paid`, `user_id`, `order_number`, `log_pohoda_id`, `log_pohoda_xml_type`, `note`, `note_to_invoice`, `storno`, `internal_exhibit`, `cost_exhibit`, `deleted`, `business_case_category_id`, `customer_id`, `currency_id`, `bill_id`, `attachment`) VALUES
(54,	'Testování odebrání položky',	NULL,	'2018-04-13 00:00:00',	NULL,	NULL,	'2018-05-13',	NULL,	1,	'55555',	'',	'',	'',	'',	0,	0,	0,	0,	1,	9,	1,	NULL,	'test.pdf'),
(58,	'3',	NULL,	'2018-04-16 00:00:00',	NULL,	NULL,	'2018-05-16',	NULL,	1,	'3',	NULL,	NULL,	'',	'',	0,	0,	0,	0,	1,	9,	2,	NULL,	'123123123.pdf'),
(59,	'Test uploadu',	NULL,	'2018-04-20 00:00:00',	NULL,	NULL,	'2018-05-20',	NULL,	1,	'123',	NULL,	NULL,	'',	'',	0,	0,	1,	0,	1,	9,	1,	NULL,	'test.pdf'),
(60,	'10101010',	123,	'2018-04-26 00:00:00',	NULL,	NULL,	'2018-05-26',	NULL,	1,	'10101010',	NULL,	NULL,	'',	'',	0,	0,	0,	0,	1,	9,	1,	NULL,	NULL),
(61,	'efad',	NULL,	'2018-04-26 00:00:00',	NULL,	NULL,	'2018-05-26',	NULL,	1,	'324',	NULL,	NULL,	'',	'',	0,	0,	0,	0,	1,	9,	1,	NULL,	NULL),
(62,	'555',	NULL,	'2018-05-07 14:11:15',	NULL,	'2018-05-07',	'2018-05-26',	NULL,	1,	'345',	NULL,	NULL,	'',	'',	0,	0,	0,	0,	1,	9,	1,	NULL,	NULL),
(63,	'Test generování OR',	NULL,	'2018-05-07 14:28:25',	'2018-05-07',	'2018-05-07',	'2018-06-03',	NULL,	1,	'55555',	NULL,	NULL,	'Test generování OR',	'',	0,	0,	0,	0,	1,	9,	1,	NULL,	NULL),
(64,	'Test',	222,	'2018-05-09 00:00:00',	NULL,	NULL,	'2018-06-08',	NULL,	1,	'222',	NULL,	NULL,	'',	'',	0,	0,	0,	0,	1,	9,	1,	NULL,	NULL),
(65,	'Test založení záznamu do DB',	NULL,	'2018-05-10 00:00:00',	NULL,	NULL,	'2018-06-09',	NULL,	1,	'999',	NULL,	NULL,	'',	'',	0,	0,	0,	0,	1,	9,	1,	NULL,	NULL);

DROP TABLE IF EXISTS `business_case_category`;
CREATE TABLE `business_case_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `business_case_category` (`id`, `name`) VALUES
(1,	'Konzultace'),
(2,	'Školení');

DROP TABLE IF EXISTS `business_case_item`;
CREATE TABLE `business_case_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_case_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `price` decimal(13,0) NOT NULL,
  `amount` decimal(8,0) NOT NULL,
  `taxes_id` varchar(8) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `business_case_id` (`business_case_id`),
  KEY `taxes_id` (`taxes_id`),
  CONSTRAINT `business_case_item_ibfk_1` FOREIGN KEY (`business_case_id`) REFERENCES `business_case` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `currency`;
CREATE TABLE `currency` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `currency` (`id`, `name`) VALUES
(1,	'CZK'),
(2,	'EUR');

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `address_id` int(10) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `ico` int(11) NOT NULL,
  `dic` varchar(50) NOT NULL,
  `contact_person` varchar(45) DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `www` varchar(50) DEFAULT NULL,
  `note` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `address_id` (`address_id`),
  CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION,
  CONSTRAINT `customers_ibfk_2` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `customers` (`id`, `user_id`, `address_id`, `name`, `ico`, `dic`, `contact_person`, `phone`, `email`, `www`, `note`) VALUES
(9,	1,	13,	'Alza.cz a.s.',	27082440,	'CZ27082440',	'David Kříž',	739198259,	'tigi.1@seznam.cz',	'',	''),
(10,	1,	14,	'Anywhere s.r.o.',	27905047,	'CZ27905047',	'David Kříž',	739198259,	'tigi.1@seznam.cz',	'',	'');

DROP TABLE IF EXISTS `log_pohoda`;
CREATE TABLE `log_pohoda` (
  `id` varchar(30) NOT NULL,
  `xml` longtext NOT NULL,
  `type` varchar(25) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `business_case_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `business_case_id` (`business_case_id`),
  CONSTRAINT `log_pohoda_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION,
  CONSTRAINT `log_pohoda_ibfk_2` FOREIGN KEY (`business_case_id`) REFERENCES `business_case` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `received_invoices`;
CREATE TABLE `received_invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_number` int(11) NOT NULL,
  `number` int(20) NOT NULL,
  `year` year(4) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `receive_date` date NOT NULL,
  `due_date` date NOT NULL,
  `variable_symbol` int(30) NOT NULL,
  `subject` varchar(80) NOT NULL,
  `amount` int(30) NOT NULL,
  `currency_id` int(3) NOT NULL,
  `deleted` int(2) NOT NULL DEFAULT '0',
  `upload` varchar(100) NOT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `based` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `approve_user_id` int(11) DEFAULT NULL,
  `approved` date DEFAULT NULL,
  `reject` date DEFAULT NULL,
  `reject-note` text,
  `prepare_to_pay` date DEFAULT NULL,
  `paid` date DEFAULT NULL,
  `state` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `currency_id` (`currency_id`),
  KEY `created_user_id` (`created_user_id`),
  KEY `approve_user_id` (`approve_user_id`),
  CONSTRAINT `received_invoices_ibfk_11` FOREIGN KEY (`approve_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `received_invoices_ibfk_6` FOREIGN KEY (`created_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `received_invoices_ibfk_8` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `received_invoices_ibfk_9` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `taxes`;
CREATE TABLE `taxes` (
  `id` varchar(30) NOT NULL,
  `vat` decimal(5,3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `taxes` (`id`, `vat`) VALUES
('21%',	0.210);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`) VALUES
(1,	'admin',	'tigi.1@seznam.cz',	'$2y$10$s6.GTdJOsvXHkN1lliuFbeWm7ZqNIqXIW7QxiCte2pZw/PT3towFy',	'admin'),
(2,	'seller',	'recepce@anywhere.cz',	'$2y$10$vNFl2nB4qcZ8WRLuf9gDPubSCNGuBhoM7EBXeHm6hYLZBoVNw6lDK',	'seller'),
(3,	'guest',	'recepce@anywhere.cz',	'$2y$10$vx/ghtS5BU.9a0KukmtwV.WUD1EOr8jOcB.j1ckUVSMqbcjDnTHKW',	'guest');

-- 2018-05-12 10:49:53
